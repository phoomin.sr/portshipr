<?php

namespace App;

use App\dbConnect;
use PDO;

class MySqlConnect implements dbConnect {
    public static function Connect($host, $port, $dbname, $username, $password)
    {
        return new PDO("mysql:host=$host;port=$port;dbname=$dbname", $username, $password);
    }
}
