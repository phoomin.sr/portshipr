<?php

namespace App;

use Exception;
use PDO;

class dbStatements {
    public $table = "";
    const fixed = "{,}";
    const SELECT  = "SELECT { all } FROM { table }";
    const INSERT = "INSERT INTO { table } VALUES ({ values })";
    const UPDATE = "UPDATE { table } SET { values }";
    const DELETE = "DELETE FROM {table}";
    const ALL = "*";

    public function query(string $operation = "")
    {
        try{
            if (!property_exists($this, 'operation')) {
                $this->operation = $operation;
            }

            switch ($this->operation) {
                case 'insert' :
                    $this->stmt = $this->replace_byval('table', $this->table,self::INSERT);
                break;
                case 'update' :
                    $this->stmt = $this->replace_byval('table', $this->table,self::UPDATE);
                break;
                case 'delete' :
                    $this->stmt = $this->replace_byval('table', $this->table, self::DELETE);
                break;
            }

            if (property_exists($this, 'preValue')) {
                $this->stmt = $this->replace_byval('values', implode(',', $this->preValue), $this->stmt);
            }
            return $this;
        } catch (Exception $e) {
            echo 'Catch Error : ', $e->getMessage(), "\n";
        }
    }
    final function set($key, $value, $clear = false)
    {
        if (method_exists($this, "set_" . $key)) {
            call_user_func_array(array($this, 'set_'.$key), array($value, $clear));
        } else {
            $this->$key = $value;
        }
        return $this;
    }
    function set_value($value, $clear = false)
    {
        if (property_exists($this, 'stmt')) {
            if (property_exists($this, 'preValue')) {
                $this->replace_byval('values', $this->preValue, $this->stmt);
            } else {
                $this->replace_byval('values', $value, $this->stmt);
            }
        } else {
            $this->preValue[] = $value;
        }
        return $this;
    }
    public function search($id = null)
    {
        $this->stmt = $this->replace_byval('table', $this->table, self::SELECT);
        $this->stmt = $this->replace_byval('all', self::ALL, $this->stmt);

        if (!is_null($id)) {
            $this->where('id', '=', $id)->exec();
        }

        return $this;
    }
    public function set_column()
    {

    }
    function fixed ()
    {
        $this->prefix = explode(',', self::fixed);
        return $this;
    }
    final function edittext(string $text)
    {
        $this->fixed();
        $prefix = $this->prefix[0] .' '. $text .' '. $this->prefix[1];
        $this->prefix = $prefix;
        return $this;
    }
    final function replace_byval($fix, $value, $tg)
    {
        $this->edittext($fix);
        return str_replace($this->prefix, $value, $tg);
    }
    function join()
    {

    }
    function where($column, $expression = null, $value = null)
    {
        $this->stmt .= " WHERE $column ";
        $this->stmt .= !is_null($expression) ? $expression:'';
        $this->stmt .= !is_null($value) ? $value:'';

        return $this;
    }
    function Like($value)
    {
        $this->stmt .= " LIKE '$value'";
        return $this;
    }
    function Equal($value)
    {
        $this->stmt .= " = '$value'";
        return $this;
    }
    function and($column)
    {

    }
    function or($column)
    {

    }
    function between($value1, $value2)
    {

    }
    function concat($arr_name, $as)
    {

    }
    function asname($as)
    {

    }
    public function exec()
    {
        try{
            if (property_exists($this, 'stmt')) {
                $stmt = $this->conn->prepare($this->stmt);
                if ($stmt->execute()) {
                    $this->status = $stmt->execute();
                    $this->row = $stmt->rowCount();
                    if ($stmt->rowCount() > 0) {
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            $this->result[] = $row;
                        }
                        return $this;
                    } else {
                        $this->status = $stmt->execute();
                        return $this;
                    }
                }
            } else {
                throw new Exception('Catch Error: statement not Found!');
            }

        } catch (Exception $e) {
            // db_error(__METHOD__, $e);
            echo 'Catch Error : ', $e->getMessage(), "\n";
        }
    }
}
