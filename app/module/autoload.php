<?php

define('BASEPATH', realpath(__DIR__));

spl_autoload_register(function ($class_name) {
    $filename = BASEPATH . '/' . strtolower(str_replace("\\", "/", $class_name));
    if (is_file($filename . '.interface.php')) {
        include $filename . '.interface.php';
    } else if (is_file($filename . '.abstract.php')) {
        include $filename . '.abstract.php';
    } else if (is_file($filename . '.class.php')) {
        include $filename . '.class.php';
    } else if (is_file($filename . '.php')) {
        include $filename . '.php';
    }
});