<?php

namespace Database;
require_once dirname(__DIR__) . "/autoload.php";

use Database\MySqlConnect;
use Database\dbStatements;

class webdatabase extends dbStatements {
    public $table = "pets";
    public function __construct($host, $dbname, $username, $password)
    {
        $this->conn = MySqlConnect::Connect($host, $dbname, $username, $password);
    }
}