<?php

class dashboard {
    const lib = <<<END_OF_TEXT
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
END_OF_TEXT;
    const prefix = '{,}';
    const load = "google.charts.load('current', {'packages':{ package }});";
    const setCallback = "google.charts.setOnLoadCallback(drawChart);";
    const Option = "var options = { option };";
    const setFunc = "function drawChart() {\n { preparingData } \n}";
    const dataTable = "var data = new google.visualization.DataTable();\n";
    // const chart = "var chart = new google.visualization.{ charttype }(document.getElementById('{ display_id }'));";
    const chart = "var chart = new google.visualization.BarChart(document.getElementById('never_land'));";
    const run = "chart.draw(data, options);";
    const addColumn = "data.addColumn({ columntype }, { columnname });\n";
    const addRows = "data.addRows({ rowstable });";
    public $type = "BarChart";
    public $option = [];
    public $packages = ['corechart'];
    public $colunm = '';
    public function __construct()
    {
        $this->start = array(
            'W' => date("Y-m-d", strtotime('monday this week')),
            'M' => date("Y-m-d", strtotime("first day of this month")),
            'Y' => date('Y-m-d', strtotime('first day of january')),
        );
        $this->end = array(
            'W' => date("Y-m-d", strtotime('sunday this week')),
            'M' => date("Y-m-d", strtotime('last day of this month')),
            'Y' => date('Y-m-d', strtotime('last day of december')),
        );
        $this->date = array(
            'W' => 'WEEK',
            'M' => 'MONTH',
            'Y' => 'YEAR',
        );
    }
    function get_load()
    {
        $load = self::load;
        // $packages = implode(',', $this->packages);
        $packages = json_encode($this->packages);

        return $this->trans('package', $load, $packages);
    }
    function get_func()
    {
        $data = self::dataTable . $this->colunm . $this->preparing_data;
        $func = self::setFunc;
        return $this->trans('preparingData', $func, $data);
    }
    function get_option ()
    {
        $data =  json_encode($this->option);
        $option = self::Option;
        return $this->trans('option', $option, $data);
    }
    function set_column ($columnarr)
    {
        foreach ($columnarr as $key => $varr) {
            $columntype = $this->trans('columntype', self::addColumn, json_encode($varr[0]));
            $column = $this->trans('columnname', $columntype, json_encode($varr[1]));

            $this->colunm .= $column;
        }
    }
    function set_row ($row)
    {
        $this->preparing_data = $this->trans('rowstable', self::addRows, json_encode($row));
    }
    function trans($search, $string, $replace)
    {
        return str_replace($this->explode($search), $replace, $string);
    }
    function explode($search)
    {
        $exfix = self::prefix;
        $fixed = explode(',', $exfix);
        return $fixed[0] . ' ' . $search . ' ' . $fixed[1];
    }
}