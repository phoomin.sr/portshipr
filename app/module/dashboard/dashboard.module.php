<?php
require_once dirname(__DIR__) . "/dashboard/core.module.php";
require_once dirname(__DIR__) . "/dashboard/visuallizeTable.module.php";

class live_dashboard extends dashboard {
    public static function getInstance()
    {
        return new live_dashboard;
    }
    function render($tg = null)
    {
        if (!is_null($tg)) {
            $this->tg = $tg;
        }

        $replace_type = $this->trans('charttype', self::chart, $this->type);
        $replace_doc = $this->trans('display_id', $replace_type, $this->tg);

        $str = $this->get_load() . "\n";
        $str .= self::setCallback . "\n";
        $str .= $this->get_func() . "\n";
        $str .= "var options = {};\n";
        $str .= $replace_doc . "\n" . self::run;

        return '<script type="text/javascript">' . $str . '</script>';
    }
}

$dashboard = new live_dashboard;
// $dashboard->type = 'PieChart';
$dashboard->set_column($data['column']);
$dashboard->set_row($data['data']);

// var_dump($dashboard->render('never_land'));

$content = "<head>"
. live_dashboard::lib
. $dashboard->render('never_land')
. "</head>";
$content .= '<body><div id="never_land"></div></body>';

echo $content;