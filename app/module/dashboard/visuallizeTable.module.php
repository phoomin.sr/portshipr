<?php

class visualize_table {
    private $column;
    function prepareTable($testData)
    {
        if(!array_key_exists(0, $testData)){
            throw new Exception('Invalid format. please use this format. example: [ ["in", "array"], ["in", "array"] ]');
        } else if (count($this->column[0]) != count($testData[0])) {
            throw new Exception('column: "' . count($this->column[0]) . '" not equal to row: "' . count($testData[0]) . '".');
        } 
        // else {
        //     $str = "";
        //     for ($i = 0;$i < count($testData);$i++) {
        //         $str .= "[" . implode(',', $testData[$i]) . "],";
        //     }
        // }
        return ['column' => $this->column, 'data' => $testData];
    }
    function addColumn($type = 'string', $name)
    {
        $this->column[] = [$type, $name];
        return $this;
    }
}

$test = new visualize_table;
$table = [
    ['lemon', 12],
    ['mint', 1],
    ['sugar', 0],
    ['fluid', 9],
];
$test->addColumn('string', 'component')->addColumn('number', 'Percent');
$data = $test->prepareTable($table);