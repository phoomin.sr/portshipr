<?php

namespace App\http\Controllers;

use App\Http\Controllers\Controller;
use App\UserModel;
use Illuminate\http\Request;

use function GuzzleHttp\json_decode;

class UserController extends Controller{
    const EMAIL = [
        'gmail' => "@gmail.com",
        'hotmail' => "@hotmail.com"
    ];
    const GENDER = [
        'mr' => 'male',
        'ms' => 'female',
        'mrs' => 'female',
    ];

    public function prepareData($data)
    {
        // fullname
        if (is_array($data['fullname'])) {
            $Fullname = $data['fullname'];
            $meta['gender'] = self::GENDER[$data['fullname']['prefix']];
            $explode = explode(' ', $Fullname['name']);
            $meta['name'] = $explode[0];
            $meta['lastname'] = $explode[1];
            $data['fullname'] = $Fullname['name'];
        }

        // email
        if (is_array($data['email'])) {
            $Email = $data['email'];
            $fullemail = $Email['topsec'] . self::EMAIL[$Email['postfix']];
            $data['email'] = $fullemail;
        }

        if (isset($data['confirm-password'])) {
            unset($data['confirm-password']);
        }

        return ['data' => $data, 'meta' => $meta];
    }

    public function AddUser(Request $request)
    {
        $httpContent = $request->getContent();
        $httpDecode = json_decode($httpContent, true)['data'];
        $post = $this->prepareData($httpDecode);
        $user = new UserModel;
        $user->User($post);
    }

}
