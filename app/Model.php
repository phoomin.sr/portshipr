<?php

namespace App;

use App\MySqlConnect;
use App\dbStatements;


class Model extends MySqlConnect {
    const HOST = "127.0.0.1";
    const PORT = "13306";
    const DBNAME = "minscale";
    const DBUSER = "root";
    const DBPASSWORD = "M1234";

    public function __construct()
    {
        $this->conn = MySqlConnect::Connect(self::HOST, self::PORT, self::DBNAME, self::DBUSER, self::DBPASSWORD);
        $this->stmt = new dbStatements;
    }
}
