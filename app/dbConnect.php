<?php

namespace App;

interface dbConnect {
    public static function Connect($host, $port, $dbname, $username, $password);
}
