-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jul 06, 2020 at 03:56 PM
-- Server version: 5.7.27
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portship`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_models`
--

CREATE TABLE `acc_models` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(54) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(54) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acc_models`
--

INSERT INTO `acc_models` (`id`, `code`, `name`, `slug`, `description`, `updated_at`, `created_at`) VALUES
(34, '100000', 'สินทรัพย์', 'ส1', '', '2020-06-29 08:25:57', '2020-06-29 08:25:57'),
(35, '110000', 'สินทรัพยหมุนเวียน', 'ส1', '', '2020-06-29 08:26:33', '2020-06-29 08:26:33'),
(36, '111000', 'เงินสดและเงินฝากธนาคาร', 'เ1', '', '2020-06-29 08:29:03', '2020-06-29 08:29:03'),
(37, '111100', 'เงินสด', 'เ1', '', '2020-06-29 08:30:12', '2020-06-29 08:30:12'),
(38, '111200', 'เงินฝากธนาคาร', 'เ1', '', '2020-06-29 08:30:39', '2020-06-29 08:30:39'),
(39, '111210', 'บัญชีเงินฝากกระแสรายวัน', 'บ1', '', '2020-06-29 08:31:39', '2020-06-29 08:31:39'),
(40, '111220', 'บัญชีเงินฝากออมทรัพย์', 'บ1', '', '2020-06-29 08:32:04', '2020-06-29 08:32:04'),
(41, '111230', 'ธนาคาร', 'ธ1', '', '2020-06-29 08:33:27', '2020-06-29 08:33:27'),
(42, '111300', 'รายการโอนระหว่างกัน', 'ร1', '', '2020-06-29 08:34:00', '2020-06-29 08:34:00'),
(43, '112000', 'ลูกหนี้การค้าและตั๋วเงินรับ', 'ล1', '', '2020-06-29 08:35:07', '2020-06-29 08:35:07'),
(44, '112100', 'ลูกหนี้การค้า', 'ล1', '', '2020-06-29 08:35:53', '2020-06-29 08:35:53'),
(45, '112110', 'ลูกหนี้การค้า-ในประเทศ', 'ล1', '', '2020-06-29 08:36:22', '2020-06-29 08:36:22'),
(46, '100000A', 'สินทรัพย์', 'ส1', '', '2020-07-06 05:44:17', '2020-07-06 05:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `acc_model_taxes`
--

CREATE TABLE `acc_model_taxes` (
  `acc_model_id` int(11) NOT NULL,
  `tax_id` tinyint(4) NOT NULL,
  `lineage` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deep` int(11) NOT NULL,
  `parent` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acc_model_taxes`
--

INSERT INTO `acc_model_taxes` (`acc_model_id`, `tax_id`, `lineage`, `deep`, `parent`, `updated_at`, `created_at`) VALUES
(34, 29, '34', 0, '', '2020-06-29 08:25:57', '2020-06-29 08:25:57'),
(35, 30, '34-35', 1, '34', '2020-06-29 08:26:33', '2020-06-29 08:26:33'),
(36, 31, '34-35-36', 2, '35', '2020-06-29 08:29:03', '2020-06-29 08:29:03'),
(37, 32, '34-35-36-37', 3, '36', '2020-06-29 08:30:12', '2020-06-29 08:30:12'),
(38, 33, '34-35-36-38', 3, '36', '2020-06-29 08:30:39', '2020-06-29 08:30:39'),
(39, 34, '34-35-36-38-39', 4, '38', '2020-06-29 08:31:39', '2020-06-29 08:31:39'),
(40, 35, '34-35-36-38-40', 4, '38', '2020-06-29 08:32:04', '2020-06-29 08:32:04'),
(41, 36, '34-35-36-38-41', 4, '38', '2020-06-29 08:33:27', '2020-06-29 08:33:27'),
(42, 37, '34-35-36-42', 3, '36', '2020-06-29 08:34:00', '2020-06-29 08:34:00'),
(43, 38, '34-35-43', 2, '35', '2020-06-29 08:35:07', '2020-06-29 08:35:07'),
(44, 39, '34-35-43-44', 3, '43', '2020-06-29 08:35:53', '2020-06-29 08:35:53'),
(45, 40, '34-35-43-44-45', 4, '44', '2020-06-29 08:36:22', '2020-06-29 08:36:22'),
(46, 41, '34-46', 1, '34', '2020-07-06 05:44:17', '2020-07-06 05:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2),
(5, '2020_06_26_153318_create_acc_models_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'phoomin', 'phoomin.sr@gmail.com', NULL, '$2y$10$lHtDWerSzm9MbI0/kiXRteqVotYgmsxzvQ71bOe2nN7EeXwl7NgF2', NULL, '2020-06-21 04:22:02', '2020-06-21 04:22:02'),
(2, 'LockOnz', 'LockOnz@gmail.com', NULL, '$2y$10$fBU3wxFqnk2/PCgZAs158uEvbbqUSHsdfXtFFz1GAyzj9KrqRBASW', NULL, '2020-06-21 08:11:44', '2020-06-21 08:11:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_models`
--
ALTER TABLE `acc_models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acc_models_code_slug_index` (`code`,`slug`);

--
-- Indexes for table `acc_model_taxes`
--
ALTER TABLE `acc_model_taxes`
  ADD PRIMARY KEY (`tax_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_models`
--
ALTER TABLE `acc_models`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `acc_model_taxes`
--
ALTER TABLE `acc_model_taxes`
  MODIFY `tax_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
