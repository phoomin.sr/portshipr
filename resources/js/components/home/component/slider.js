import React from 'react';
import { Carousel } from 'antd';

const Carouselslider = () => {
    return (
        <Carousel autoplay>
            <div>
                <h1>1</h1>
            </div>
            <div>
                <h1>2</h1>
            </div>
            <div>
                <h1>3</h1>
            </div>
            <div>
                <h1>4</h1>
            </div>
            <div>
                <h1>5</h1>
            </div>
        </Carousel>
    );
}

export default Carouselslider;
