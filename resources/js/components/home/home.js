import React from 'react';
import { Carousel, Row, Col, Divider, Typography } from 'antd';
import Carouselslider from './component/slider';

const { Paragraph } = Typography;

const HomePage = () => {
    return (
        <>
            <Row>
                <Col>
                    <Carouselslider />
                </Col>
            </Row>
            <Row>
                <Col span={2}></Col>
                <Col span={20}>
                    <Divider orientation='left'>minscale's erp</Divider>
                    <Paragraph ellipsis={{ rows: 4, expandable: true, symbol: 'more' }}>
                        minscale ให้บริการ ERP สำหรับธุรกิจที่ต้องการบริหาร ทรัพยากรต่าง เพื่อให้รับรู้ข้อมูลที่แท้จริงของธุรกิจ นอกจากนี้
                        ระบบ ERP ยังแก้ปัญหาการซ้้ำซ้อนของข้อมูล หรือการมีข้อมูลที่อัพเดทล่าช้า นอกจากนี้ระบบของ minscale ยังเน้นการ
                        ทำงานร่วมกัน แบบ realtime มั่นใจได้ว่าทุกคนที่ทำงานร่วมกันจะไม่มีการทำงานที่ซ้ำซ้อน ระหว่างที่ทำงานทำให้งาน
                        เสร็จได้ในเวลาอันรวดเร็ว ถูกต้อง และแม่นยำ

                        minscale เป็นโปรเจ็คสร้าง platform สำหรับให้การบริหารธรุกิจเต็มรูปแบบโดยบัจจุบันอยู่ใน pharse การสร้าง
                        ระบบ 'ERP' + 'Accounting system' โดยในอนาคต minscale วางแผนที่จะวางระบบ smart office ที่ใช้
                        จัดการระบบไฟฟ้าและระบบความปลอดภัยสำหรับออฟิศที่เชื่อมโยงออฟฟิศของคุณเข้าสู่ระบบอินเตอร์เน็ต สามารถควบคุม
                         office จากทุกอุปกรณ์เพิ่มความสะดวกให้กับยุค Iot

                        นอกจากนี้ minscale ยังต้องการพัฒนาระบบ data Analysis สำหรับให้บริการข้อมูลให้กับองค์กรของคุณอีกด้วย โดย
                        ทั้งหมดที่กล่าวมานี้ คือแผนในการเริ่มต้นของเราเท่านั้น
                    </Paragraph>
                </Col>
                <Col span={2}></Col>
            </Row>
        </>
    );
}

export default HomePage;
