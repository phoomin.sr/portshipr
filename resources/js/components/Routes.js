import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import { Menu } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import HomePage from './home/home';
import NormalLoginForm from './login/login';
import NormalRegisForm from './signup/signup';

class AppMenu extends React.Component {
  state = {
    current: 'home',
  };

  handleClick = e => {
    this.setState({ current: e.key });
  };

  render() {
    const { current } = this.state;
    return (
        <Router>
            <Menu onClick={this.handleClick} selectedKeys={[current]} mode="horizontal">
                <Menu.Item key="home" icon={<MailOutlined />}>
                    <Link to="/">
                        Home
                    </Link>
                </Menu.Item>
                <Menu.Item key="service" icon={<AppstoreOutlined />}>
                    <Link to="/service">
                        Services
                    </Link>
                </Menu.Item>
                <Menu.Item key="document" icon={<AppstoreOutlined />}>
                    <Link to="/doc">
                        Doc.
                    </Link>
                </Menu.Item>
                <Menu.Item key="login">
                    <Link to="/login">
                        SignIn
                    </Link>
                </Menu.Item>
            </Menu>
            <Switch>
                <Route path="/service">
                    {/* <Service /> */}
                    <h1>services</h1>
                </Route>
                <Route path="/doc">
                    {/* <Doc /> */}
                    <h1>doc.</h1>
                </Route>
                <Route path="/login">
                    <NormalLoginForm />
                </Route>
                <Route path="/register">
                    <NormalRegisForm />
                </Route>
                <Route path="/">
                    <HomePage />
                </Route>
            </Switch>
        </Router>
    );
  }
}

export default AppMenu;
