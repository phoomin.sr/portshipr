import React from 'react';
import ReactDOM from 'react-dom';
import { Layout } from 'antd';
import AppMenu from './Routes';
// import App from './index';
import 'antd/dist/antd.css';
import './index.css';

const { Content } = Layout;

function Home() {
    return (
        <Layout>
            <Layout>
                <Content>
                    <AppMenu />
                </Content>
            </Layout>
        </Layout>
    );
}

export default Home;

if (document.getElementById('home')) {
    ReactDOM.render(<Home />, document.getElementById('home'));
}
