import axios from 'axios';
import base_url from '../../config';

const url = base_url('development');

const ajax = () => {
    return url;
}

export default ajax;
