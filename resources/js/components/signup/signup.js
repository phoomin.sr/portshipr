import React from 'react';
import { Form, Input, Button, Checkbox, Row, Col, Typography, Select } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import MD5 from 'crypto-js/md5';
import ajax from '../module/ajaxPost/ajax';
import { Link } from 'react-router-dom';
import FormItem from 'antd/lib/form/FormItem';

const { Title } = Typography;
const { Option } = Select;
const instance = axios.create({
    baseURL: 'http://localhost:8000/laravel',
    method: 'POST'
});

const NormalRegisform = () => {
    const Box = props => <div className={`mt-${props.value}`}>{props.children}</div>;

    const onFinish = values => {
        if (values['password'] == values['confirm-password']){
            instance.post('/addUser', {data: values})
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        }
    }
    return (
        <>
            <Row>
                <Col span="4"></Col>
                <Col span="16">
                    <Box value="5">
                        <Form
                        name="normal_regist"
                        className="login-form"
                        onFinish={ onFinish }>
                            <Row justify="center">
                                <Title>SignUp</Title>
                            </Row>
                            <Box value="5">
                                <Form.Item>
                                    <Input.Group compact>
                                        <Form.Item
                                            name={['fullname', 'prefix']}
                                        >
                                            <Select placeholder="title">
                                                <Option value="mr">Mr.</Option>
                                                <Option value="ms">Ms.</Option>
                                                <Option value="mrs">Mrs.</Option>
                                            </Select>
                                        </Form.Item>
                                        <Form.Item
                                            name={['fullname', 'name']}
                                        >
                                            <Input placeholder="name - lastname" />
                                        </Form.Item>
                                    </Input.Group>
                                </Form.Item>
                            </Box>
                            <Box value="2">
                                <Form.Item>
                                    <Input.Group compact>
                                        <Form.Item
                                            name={['email', 'topsec']}
                                        >
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['email', 'postfix']}
                                        >
                                            <Select placeholder="@email.com">
                                                <Option value="gmail">@gmail.com</Option>
                                                <Option value="hotmail">@hotmail.com</Option>
                                            </Select>
                                        </Form.Item>
                                    </Input.Group>
                                </Form.Item>
                            </Box>
                            <Box value="2">
                                <Form.Item name="password" >
                                    <Input.Password placeholder="password" />
                                </Form.Item>
                            </Box>
                            <Box value="2">
                                <Form.Item name="confirm-password" >
                                    <Input.Password placeholder="confirm-password" />
                                </Form.Item>
                            </Box>
                            <Form.Item>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                >
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                    </Box>
                </Col>
                <Col span="4"></Col>
            </Row>
        </>
    );
}

export default NormalRegisform;
