const base_url = (locate) => {
    let http = {
        'development' : 'http://localhost:8000/laravel/',
        'production' : 'https://minscalses/laravel/'
    };

    return http.locate;
}

export default base_url;
